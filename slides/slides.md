---
marp: true
theme: gaia
class: invert   
paginate: true
auto-scaling: true
footer: Arsène Fougerouse D03 
backgroundColor: #fff
backgroundImage: url('https://wallpapercave.com/wp/wp4667228.jpg')
--- 
<!-- paginate: false -->
![bg left:40% width:100% auto](https://cdn.programiz.com/sites/tutorial2program/files/lcs-X.png)

# **Algorithme LCS**

**Arsène Fougerouse**

_En 180 secondes chrono :bomb: !_

---

## **Concept**

LCS *Longuest Common Subsequence* : plus longue sous-séquence commune entre deux chaîne de caractères.  
:warning: Ne prends pas en compte l'espacement  


#### Exemple :

(DEVOPS) et (DOCKS) : (DOS)

--- 

## **Algorithme**

- *D* et *O* : deux chaînes de caractères de taille *m* et *n*.
- *L* : Tableau à 2 dimensions L(m+1, n+1)
- chaque case = taille LCD(*D*,*O*)

```c++
for (int i=0; i<=m; i++)
	for (int j=0; j<=n; j++)
        if(i == 0 || j == 0)
            L[i][j] == 0;
        if (D[i-1] == O[j-1])
            L[i][j] = L[i-1][j-1] + 1;
        else
            L[i][j] = max(L[i-1][j], L[i][j-1]);
```

---
<style scoped>
    strong{
        color:#7ccdf1;
    }
</style>

## **Etapes détaillées**
**j\i**||D |E|V| O | P | S
|---|---|---|---|---|---|---|---
| |0|0|0|0|0|0|0
|D|0|
|O|0|
|C|0|
|K|0|
|S|0|


---
<style scoped>
    strong{
        color:#7ccdf1;
    }
</style>
**j\i**| |D |E|V| O | P | S
|---|---|---|---|---|---|---|---|
| |0|0|0|0|0|0|0
D|0|**1**|1|1|1|1|1
O|0|
C|0|
K|0|
S|0|

---
<style scoped>
    strong{
        color:#7ccdf1;
    }
</style>

**j\i**| |D |E|V| O | P | S
|---|---|---|---|---|---|---|---|
| |0|0|0|0|0|0|0
D|0|**1**|1|1|1|1|1
O|0|1|1|1|**2**|2|2|
C|0
K|0
S|0

---
<style scoped>
    strong{
        color:#7ccdf1;
    }
</style>

**j\i**|  |D |E|V| O | P | S
|---|---|---|---|---|---|---|---|
| |0|0|0|0|0|0|0
D|0|**1**|1|1|1|1|1
O|0|1|1|1|**2**|2|2|
C|0|1|1|1|2|2|2|
K|0|1|1|1|2|2|2|
S|0|1|1|1|2|2|**3**

---
<!--
footer: " "
-->
<style>
pre{
    /*height:15%;*/
    width:75%;
}
</style>

# **Callback algorithm**



```c++
int index = L[m][n];  //index = taille du LCS
char lcs[index+1]; 
lcs[index]='\0';
int i = m, j = n;

while (i > 0 && j > 0)
{
	// Caractère égaux, donc partie de la LCS
	if (D[i-1] == O[j-1])
	{
		lcs[index-1] = D[i-1]; // Place charactère dans la LCS
		--i; --j; --index;
	}
    //Sinon trouve le plus grand et va dans sa direction
	else if (L[i-1][j] > L[i][j-1])
		--i;
	else
		--j;
}
```
---
<style scoped>
strong{
    color:#7ccdf1;
    }
em {
    background-color: red;
    padding: 0 4px;
    font-weight: bold;
}

</style>
# **Callback algorithm**
**j\i**|  |D |E|V| O | P | S
|---|---|---|---|---|---|---|---|
| |*0*|0|0|0|0|0|0
D|0|**1**|*1*|*1*|1|1|1
O|0|1|1|1|**2**|*2*|2|
C|0|1|1|1|2|*2*|2|
K|0|1|1|1|2|*2*|2|
S|0|1|1|1|2|2|**3**

--- 
# **Utilisations**

![bg left auto](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEX///8AAADMzMzS0tI+Pj5cXFysrKzi4uKOjo7T09PPz8/5+fnm5ubCwsLLy8u+vr7s7OxSUlJ7e3u2trZjY2NNTU0xMTGXl5fd3d1paWnx8fEmJiZwcHAUFBREREQgICClpaV2dnY7OzsLCwuAgICIiIgXFxcrKyumpqadnZ00NDRcupVGAAAJyElEQVR4nO2d23riOgxGc+DQJCQUKKRAodAzvP8Dbhhoi2TFkcBK9szn/2YuhuIsLFuyLStB4OXl5eXl5eXl5eXl5eXl5eXl5eXl5eXl9TcqK3rj0ctsNtuv5+Vw0PbjuNZjOQuRnoZZ20/lTNlkjfFOGj+2/WhuVC5pvqO6SdtPd7smq2q+o+aLtp/wNt2P7HxHTdp+yFs0qec76OHvnXLuWIBh+Cn3HYO7aYeh9TZV4PrRnAl4kHTC4f52B430LEQAGIayn3or+epnLUQRYBhKXGNP9tVzHUCBHf3RSuA1hF8t+vXY4s2il+qwv7uQfvW7AuBADBiGd9wvFxrpwR0pEE7JlkbjsjecbN7ndBzHnVD/D4SUjXbKKI7Tow7/TJ6ITyy1CLfOATOzkf1Xkka/SpOCYNwoEQ6dE5ZGG+P0ku+Pkp6xZPzgeS4p4dK5Q1wYy4lNjPmO/VgYo3WjQhi7Bgy+cBMTCvDYjXjlMWP92kLC3DlggFf0ZA+euhF/tHBN+LFV2BF6RI2MKwGjdIgM+ukWwtnjoI+lsr5G88yLMcdcKMaT0i2E4VQDh9AeNtuzEUZJB36aY6bVVrpuZC2NnOE6sQFG6QZ+vGS0YBmHiovBX6G4eGftwkMnwpHIMTTbTNOEoSJfUQMYxWP4eUYL1rl0rQ4YbKHZVE+kZzNFz3t/I2E4VTdUGKi81RFGOXxAxgKjxh+qj0UYbdYNwygq4B8wJtM6j689Fp9Ba5NawhR2OiPGqo1plMciJBzWEsbd2wip5bSuoUr7MIbht5iwvH9tGBH+pF/1VgqjbzHhXdAnEDUN9QW2Xz/TwE6P5ITB4MNEVHQacFg9Sb0FY2/TICS39vQMFW4Ff9Z6fLS6YDyXSUgaqprTQPtsdZNpAvv8mdECQUgaqtZYRAvgbU0nokCdswSmCIMBcdasNRZhK6+FvQtR4N27ljAYNOc00Ebo2LZATNE8wwm8KwiDvkmoNBaHqBWb00cBDe94poKQHosavbhAjeyr7TTGZ3C7WwibM9QtbqTKTmMjhuYYaTUh6TS6CoSJ0Yi5p08D8k5rqwmDPjGjaiAaSTTT3PQZqWGi3PM1CyE5FsfuCY1ODD92CerGJO8an2Ke89kIybHIcUFCmQ8fTntJ/AOZJvmYeJK+A0JyLLrPnrs3GzkEZG+TIjoejxZ5SfwE/ON2OyFlqCv3E6px/HTWfvTQnT7T//fJ/fIaQiqAU0g4eahAtIhtSjC2J3bJCUN1fwy8IOY0uzbs74ZjgPphTESFxCgqSrRJMqVfjmJ6iWQaqkJOTSoCFA2UyyV9hW2bPzBzopYILxucAQZB/POHlRtXEW5CI20IHwZX60361YvTEm1k6RgjJNTIbrvvGCykWCsKpEWR5/Y4Ha2tVToxCN4YfEutfP1P1JDOLZb6PMI3tV2/GLWkkaJ4lJkgdamR5qWSLWxLLVl4scPm8qMH1TxzIzx2H9h8KxtSKdH7UsFFQaEVqFI69ElZDqe2Wa+J22u4E3VPh+GyWGNrgRDyGHpmelTcBiEKOsSRhUitECKfuFJtqx3CHexE1Yty7RCiNYZCrumv2iFEaQXXRMBstUS4Bc3qRN9ntUQIcx9fNJtqiRC6Yfdp+xdqiRCFNZqRVEuEaNNPcynTEmEG83s0b8e3RIiyQf9FwtE/TwgPgdzfEPqVJ9SSJ3QnT6glT+hOnlBLntCdPKGWPKE7eUIteUJ3gpteTV0sb4/wpbEabc0RwoOu58aKCTZHiG5fsXLWXag5wgxeTGys6GVzhOhi4kazqUs1SAiTat3XbqpQg4TwOJZzDc+JGiRE2ZhN1UpukBCdAqkex16oQcIAJl+zU/NvVJOEKBWzoULQTRKii4kK15EoQUJdN4xL8TUR1gxQ3btxrhoQo/uz7GKX12pQoppVfzRXLAaP78/qdmJaeW9nWWrF/Rm68aQZ1/TtlcQ5Fbeu0TtqRy1FOMMtmWKV9xML16/QSm9JLKX8f6STQo+Hht1jDPK8uGbEbEgiQy8a84BxjcZygfU8kubi5+Bc8jhJwzsa81tlPZrfiVcYh0gKiStkY5p3yiqCt8vNR9EyhKpQXC1GwR+pjEluRT//ZW0lSc4k30RPcn8jAvvEikbg/jF/KDInmYsf2L3zx4FNGL4SiPAuEfslH2YlgFop7L6bMwGBCG+7sEsGVNw/ep52H7ov9P+FX07pjsrMy+TmWLyOkIxkuru8SOM4jorJGxkJuHeLlCnhXryKkLhd/fpW/NarSOOkR9TDV7BTqvAh6sWrCM3FRBeXHEmTnTnVKTh+YkpfwV68htC0jbvYLBsT50Y3jtwTUqVB4HRzDaExhfXIOmNpbPS1RvRGlf64RLyC0Cg3QgNGRo04nYVqRi1QL8biFYToitPBRCsAo6jAnkNlIUW8be7CaVxBiK7Gdy2l8FL8Ag6di4nUe0x+DFVOiIz0NbeVM8SF8FjV7sUiDfUbUU6IXJC1EN7BTlH1ExVCEvHbacgJ0arJXswwirfw41ob8NRY7F9JCB15196FUYoWAGov8Ks0VDEh2lAva2vBwyBdbXe60mmICdEOUF4DGMXQqvUymDJiRj0aqpgQFuB4rhmGBzOFLWheTCTG4iGAExPCE+Z1fQlqOPdy3zV1lQhDfb2Ha3wxoc3dnwmh01dNK6DG4hJG0RqEcDJVTpyoeF3wDYQjqZVqp4bQL6ITEcKZZlY/08BxMFMmJJ2GjBAtf+u9BVyFq2eD1iAyCFHgbX9DUWS8sKCBFC3rWOREbfAvLK/ROlupvIVbZetFTvtwLK/q4lJUvkrnvBTKZqgcQvTIG7uZJshmmnkVVvWMyiFEddM61k5MUYHVvTrdSZWIrFGC/qa0jkSUhqKSuLAw3nM3eKTSX9iEaAm8sryVAb9GS2MBPNhKSriyCHGtzeroO8bbcgrviTCP2G4nzPB7TUe4wvY3oHGq4N5XyKq3cp/AOB2dFhSi0YMK5QYzTrqLnDAzDP+zZ8yoaYTHoMY8I7RRthURR9xPOXoP8Zfxjl4NZ7hVIgyoSuHzSfr9Lum0KKmDYIV9NnGxby4hrlh60nL+vplMeuW24m3h7gH1CHGxS5Y0Lg3UJw4i8ePi2u0CQyp7wfwi0Wfxv1pcC15pK1hophKHLKwFr1W0lUiMskh29EXPNg0DHhDJF1zQktqRIC9K59jwrHi77jA0vZNPdfeEU3fy2/1/lPFSTNVO1JoQPqcnNGrsnq6OFjWJtKu/ugNPerS4pGXZ2GV5VfUr8oXXvX+D76hsuMV4+516JfimNRiW8/V+Npt1RuOeQuK6l5eXl5eXl5eXl5eXl5eXl5eXl5eXl5dXE/oPWeWCYNShmWYAAAAASUVORK5CYII=)

- Git Diff

---
<style scoped>
    h1{
        position:absolute;
        right: 8rem;
        top:5rem;
    }
</style>
# **Merci de votre attention**