# Marp-presentation

Little project to build an website page for an demonstration using R2Devops jobs.

*Made with ❤ using [Marp](https://r2devops.io/jobs/build/marp/) and [Page](https://r2devops.io/jobs/deploy/pages/)*

---
Author : [@GridexX](https://gitlab.com/GridexX)